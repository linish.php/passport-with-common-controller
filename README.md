## About Passport With Common Controller

This is a sample application which uses Passport and Laravel UI Auth with Bootstrap to serve the output based on the request from a common controller.

Basically if the request is coming from API the controller will return JSON

Else it will load the view

This is done with laravel 8

This project uses sqlite so not db creation required

## Setup Instructions

 - Copy the file `database.sqlite` from `DB_DUMP` folder into `database` folder
 - Copy and rename the file `.env-copy-and-rename` to `.env`
 - `php artisan serve`
 - Register a new user
 - API Endpoint to login `yoururl/api/login`
 - API Endpoint to get products `yoururl/api/products`
 - Frontend url to get products after login `yoururl/products`

## Outputs

### Passport Output
![Passport Output](output-demo/passport-output.png)

### Browser Output
![Browser Output](output-demo/browser-output.png)


## References
 [Project Base Setup](https://www.itsolutionstuff.com/post/laravel-8-rest-api-with-passport-authentication-tutorialexample.html)

 [Passport Setup Reference](https://laravel.com/docs/8.x/passport#passport-or-sanctum)